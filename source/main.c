#include <stdio.h>
#include <stdlib.h>


void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int cant);
void enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mask, int cant);

void open_file(char *file_name, int cant, unsigned char *buff)
{
    FILE *file_open;
    file_open = fopen(file_name,"rb");

    if (file_open == NULL)
    {
        printf("Archivo no cargado.\n");
    }
    else
    {
        fread(buff, cant, 1, file_open);
    }

    fclose(file_open);   
}

void saved_file(char *file_name, int cant, unsigned char *buff)
{
    FILE *file_save;
    file_save = fopen(file_name,"wb");
    fwrite(buff, 1,cant, file_save);
    fclose(file_save);
}

void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int cant)
{
    /* Tendremos en cuenta lo siguiente:
        Si el byte a chequear es 0 -> negro
        Si el byte a chequear es 255 -> blanco
     */ 
    for(int i =0 ; i < cant; i++)
    {
        if(mask[i] != 0)
        {
            a[i] = b[i];
        }
    }
}

int main(int arg, char* argv[])
{

    printf("Iniciando TP 2 \n");
    printf("Cargando archivos \n");
    //Iniciamos la configuracion de los parametros a usar
    char *image_a = argv[1];
    char *image_b = argv[2];
    char *mask = argv[3];
    
    /*Con la funcion atoi convertimos una cadena de caracteres
    en un valor entero int con lo cuales tendremos el ancho y el alto */
    int ancho = atoi(argv[4]);
    int alto = atoi(argv[5]);
    int cant_pixel= 3;
    int cant = ancho * alto * cant_pixel;
    
    /* Iniciamos la reserva de memoria dinamica con malloc
     */
    printf("Iniciando reserva de memoria dinamica \n");
    unsigned char *a = malloc(cant); 
    unsigned char *a_asm = malloc(cant);
    unsigned char *b = malloc(cant);
    unsigned char *mascara = malloc(cant);

    /* Accedemos a las imagenes
     */
    printf("Accediendo a las imagenes \n");
    open_file(image_a, cant, a);
    open_file(image_b, cant, b);
    open_file(image_a, cant, a_asm);
    open_file(mask, cant, mascara);

    /* Iniciamos el proceso de imagenes desde la funcion en C.
     */
    printf("Iniciando enmascado en C \n");
    enmascarar_c(a,b, mascara, cant);
    printf("Iniciando enmascarado en ASM \n");
    enmascarar_asm(a_asm,b, mascara, cant);
    printf("Guardando resultados \n");
    saved_file("enmascarado_c.rgb",cant , a);
    saved_file("enmascarado_asm.rgb",cant , a_asm);
    /* Liberamos la memoria dinamica asignada con malloc usando el metodo free
     */
    printf("Liberando memoria dinamica usada \n");

    free(a);
    free(a_asm);
    free(b);
    free(mascara);
    printf("Proceso TP2 finalizado \n");
    return 0;
}
