;%include "io.inc"

section .data
    cantidad dd 0

section .text
    global enmascarar_asm

enmascarar_asm:
    push ebp ; direccionamos a la base de la pila
    mov ebp, esp

    mov ecx, 0 ;inicializamos nuestro contador para ciclar
    mov edx, [ebp+20] ; puntero a la direccion de memoria de la variable cant
    mov [cantidad], edx

ciclo:
    cmp ecx, [cantidad]
    je salir

    ;almacenamos la imagen a
    mov eax,[ebp+8]
    movq mm0,[eax+ecx]
    ;almacenamos la imagen b
    mov eax,[ebp+12]
    movq mm1,[eax+ecx]
    ;almacenamos la mascara
    mov eax,[ebp+16]
    movq mm2,[eax+ecx]

    ;iniciamos con la logica para el proceso de imagenes
    ;comparamos los pixeles de la imagen b, en caso de ser negro se mantiene
    pand mm1,mm2;
    ;comparamos los pixeles de la imagen a, en caso de ser blanco se mantiene
    pandn mm2,mm0;
    por mm1, mm2

    ;retornamos la primer imagen la cual es procesada
    mov eax,[ebp+8]
    movq [eax+ecx], mm1

    add ecx, 8
    jmp ciclo

    salir:
        mov esp, ebp ;Restauramos la pila
        pop ebp
        ret