# TP2-OC2 - Fabian Mamani

**Ejecución**

    
- Primero lo que debemos hacer es ejecutar nuestro archivo .sh
        
        ./script.sh

-  Luego ejecutaremos el archivo main que creamos al ejecutar el archivo .sh
        
        ./main img_a.rgb img_b.rgb img_mask.rgb 300 300

    _En caso de querer imagenes con distinto tamaño debemos cambiar los ultimos parametros._
    
    _Por ejemplo:_

    _./main img_a.rgb img_b.rgb img_mask.rgb 400 700_

- Finalmente ejecutaremos el siguiente comando para cambiar el formato de rgb a jpg
        
        gm convert -size 300x300 depth 8 enmascarado_c.rgb retorno_c.jpg

        gm convert -size 300x300 depth 8 enmascarado_asm.rgb retorno_asm.jpg

- Para visualizar las imagenes ejecutaremos la siguiente linea

        gm display retorno_c.jpg

        gm display retorno_asm.jpg





